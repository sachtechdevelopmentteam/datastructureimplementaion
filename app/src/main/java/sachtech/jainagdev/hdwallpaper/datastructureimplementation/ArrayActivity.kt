package sachtech.jainagdev.hdwallpaper.datastructureimplementation

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import sachtech.jainagdev.hdwallpaper.datastructureimplementation.array.Array
import sachtech.jainagdev.hdwallpaper.datastructureimplementation.array.DeleteElement
import sachtech.jainagdev.hdwallpaper.datastructureimplementation.array.MergeSort

class ArrayActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_array)
        Array().insertion()
        DeleteElement().deletion()
        var array= intArrayOf(12,32,11,22,34,99,67)
        MergeSort().mergeSort(array,0,6)
        var item=34
        Array().binarySearch(array,0,6,item)


    }
}
