package sachtech.jainagdev.hdwallpaper.datastructureimplementation.linkedlist

import android.content.Context
import android.widget.Toast


class LinkedList {
    var head: Node? = null
    class Node{
        var data: Int = 0
        var next: Node? = null
        constructor(d:Int){
            data=d

        }
    }

    fun insert(list:LinkedList,data:Int):LinkedList{
        val new_node = Node(data)
        new_node.next = null
        if (list.head == null) {
            list.head = new_node;
        }
        else{
            var last = list.head
            while (last?.next != null) {
                last = last.next

            }


            last?.next = new_node
        }
        return list
    }

    fun deleteAtPosition(list:LinkedList,index:Int,activity: Context):LinkedList{

        var currentNode=list.head
        var previousNode:Node?=null
        if(index==0 && currentNode!=null){
            list.head=currentNode.next
            return list
        }
         var counter=0
        while(currentNode!=null){
            if(counter==index){
               previousNode?.next=currentNode.next
                break
            }
            else{
                previousNode=currentNode
                currentNode=currentNode.next
                counter++
            }
        }
        if(currentNode==null){
            Toast.makeText(activity,"No Items Found",Toast.LENGTH_LONG).show()
        }
        return list
    }

    fun deleteByValue(list: LinkedList,value:Int,activity:Context):LinkedList{
        var currentNode=list.head
        var previousNode:Node?=null
        if(currentNode!=null && currentNode?.data==value){
            list.head=currentNode.next
        return list}
        while(currentNode!=null && currentNode?.data!=value){
            previousNode=currentNode
            currentNode=currentNode.next
        }
        if(currentNode!=null){
            previousNode?.next=currentNode.next
        }
else if(currentNode==null){
            Toast.makeText(activity,"Data not found",Toast.LENGTH_LONG).show()
        }
        return list
    }

    fun insertAtPosition(list:LinkedList,index:Int,value:Int,activity: Context):LinkedList{
        val new_node=Node(value)

        if(index!=0 && list.head==null) {
          Toast.makeText(activity,"Found empty list. So, cannot be placed at"+" "+index,Toast.LENGTH_LONG).show()
        }
        var counter=0
         if(index!=0 && list.head!=null)
        {
          var currentNode=list.head

            while (currentNode!=null){
                 if(counter==(index-1)){
                   var tempNode= currentNode?.next
                   currentNode?.next=new_node
                   new_node.next=tempNode
                     break
               }
                if(counter!=(index-1)) {
                    currentNode = currentNode.next
                    counter++
                }

            }
            if(currentNode==null){
                Toast.makeText(activity,"Cannot be placed at"+" "+index,Toast.LENGTH_LONG).show()
            }

        }
        return list

    }

    fun getValue(list:LinkedList,index:Int,activity: Context):Int{
        var currentNode=list.head
        var value:Int?=0
        if(list.head==null){
            Toast.makeText(activity,"No Data Found",Toast.LENGTH_LONG).show()
        }
        var counter=0
        while (currentNode!=null){
            if(counter==index){
               value=currentNode.data
                break
        }else{
                currentNode=currentNode.next
                counter++
            }
        }
        if(currentNode==null) {
            throw MyException("ArrayIndexBoundOutOfException")
        }

            return value!!
    }


}