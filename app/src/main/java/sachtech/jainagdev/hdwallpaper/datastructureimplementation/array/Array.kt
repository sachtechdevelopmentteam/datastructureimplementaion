package sachtech.jainagdev.hdwallpaper.datastructureimplementation.array

class Array {

    var index:String?=null
    var number:String?=null
    var indexValue:Int?=null
    var numberValue:Int?=null

    fun insertion(){
        var value= arrayOf(11,22,3,44,55)
        indexValue=3
        numberValue=45
        var size=value.size-1
        var position=size
        var result=value.copyOf(size+2)
        while (position>= indexValue!!){
            result[position+1]=value[position]
            position=position-1
        }
        result[indexValue!!]= numberValue!!

        //number_value.text= result[0].toString()+","+result[1].toString()+" , "+result[2].toString()+ " ," +result[3].toString()
    }


    fun binarySearch(arr: IntArray, start: Int ,end: Int, item: Int): Int {
        if (end >= start) {
            val mid = start + (end - start) / 2
            if (arr[mid] == item)
                return mid
            return if (arr[mid] > item) binarySearch(arr, start, mid - 1, item) else binarySearch(arr, mid + 1, end, item)

        }
        return -1
    }
}