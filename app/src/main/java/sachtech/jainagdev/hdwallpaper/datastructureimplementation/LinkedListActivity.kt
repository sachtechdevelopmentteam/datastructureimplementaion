package sachtech.jainagdev.hdwallpaper.datastructureimplementation

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import sachtech.jainagdev.hdwallpaper.datastructureimplementation.linkedlist.LinkedList

class LinkedListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var linkedList= LinkedList()

        linkedList.insert(linkedList,4)
        linkedList.insert(linkedList,7)
        linkedList.insert(linkedList,78)
        linkedList.insert(linkedList,89)
        linkedList.deleteAtPosition(linkedList,2,this)
        linkedList.deleteByValue(linkedList,89,this)
        linkedList.insert(linkedList,67)
        linkedList.insert(linkedList,84)
        linkedList.insertAtPosition(linkedList,2,56,this)
        linkedList.getValue(linkedList,2,this)

    }
}
