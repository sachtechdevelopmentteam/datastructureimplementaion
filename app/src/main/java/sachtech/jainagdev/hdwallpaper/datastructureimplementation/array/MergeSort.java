package sachtech.jainagdev.hdwallpaper.datastructureimplementation.array;

public class MergeSort {


   public void mergeSort(int Arr[], int start, int end) {

        if (start < end) {
            int mid = (start + end) / 2;
            mergeSort(Arr, start, mid);
            mergeSort(Arr, mid + 1, end);
            merge(Arr, start, mid, end);
        }
    }

    void merge(int Arr[],int start,int mid,int end){
        int n1 = mid - start + 1;
        int n2= end-mid;

        int L[] = new int [n1];
        int R[] = new int [n2];

        for (int i=0; i<n1; ++i)
            L[i] = Arr[start + i];
        for (int j=0; j<n2; ++j)
            R[j] = Arr[mid + 1+ j];

        int i = 0, j = 0;

        int
         k = start;
        while (i < n1 && j < n2)
        {
            if (L[i] <= R[j])
            {
               Arr[k] = L[i];
                i++;
            }
            else
            {
                Arr[k] = R[j];
                j++;
            }
            k++;
        }


        while (i < n1)
        {
            Arr[k] = L[i];
            i++;
            k++;
        }


        while (j < n2)
        {
            Arr[k] = R[j];
            j++;
            k++;
        }

    }
}
