package sachtech.jainagdev.hdwallpaper.datastructureimplementation

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import sachtech.jainagdev.hdwallpaper.datastructureimplementation.arraylist.MyArrayList

class ArrayListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_array_list)
        var arrayList= MyArrayList()
        arrayList.add(22)
        arrayList.add(44)
        arrayList.get(1)
        arrayList.remove(1)
    }
}
